# Customisable delete confirmation button Component #

This package contains a simple Vue component: a button that when clicked will ask user for confirmation before deleting a resource.
If provided, the HTML element representing the resource will fade out.

### Installation ###
`npm i @ianfusion/vue-delete-button`

then in app.js add the line:
"import VueDeleteButton from '@ianfusion/vue-delete-button';"

and add

var vm = new Vue({
    el: '#app',
    components: {
        VueDeleteButton,
    },
});

to import it.

### Requirements ###
 - Sweet alert
 - jQuery

### Support ###

If you come across any issues feel free to email me: ian@infusion-it.co.uk.

### License ###

The MIT License (MIT). Please see the [License File](LICENSE) for more information.
