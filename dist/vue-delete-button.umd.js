(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.DeleteButton = {})));
}(this, (function (exports) { 'use strict';

    (function(){ if(typeof document !== 'undefined'){ var head=document.head||document.getElementsByTagName('head')[0], style=document.createElement('style'), css=""; style.type='text/css'; if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); } head.appendChild(style); } })();




    var component = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{on:{"click":function($event){_vm.deleteItem();}}},[_c('i',{staticClass:"fas fa-trash-alt"}),_vm._v(" Delete")])},staticRenderFns: [],
        props:[
            'targetUrl',
            'parentToRemove' ],

        data: function data() {
            return {
                transitionSpeed: 400, //ms
            }
        },

        methods:{
            deleteItem: function deleteItem($event){
                var this$1 = this;

                console.debug('deleteItem method called', event);
                var clickedButton = event.target;
                swal({
                    title: "Are you sure?",
                    text: "This action cannot be undone.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonText: "Yes",
                })

                .then(function (userConfirmsDeletion) {
                    if (userConfirmsDeletion) {
                        axios.post(this$1.targetUrl, {
                            _method: "DELETE",
                        })
                        .catch(function (error) {
                            swal("Error", error.response.data.message, "error");
                        })
                        .then(function (response) {
                            $(clickedButton).closest(this$1.parentToRemove).hide(this$1.transitionSpeed);
                            swal("Success", response.data.message, "success");
                        });
                    }
                    else{
                        e.preventDefault();
                    }

                });
            },
        },
    }

    // Import vue component

    // Declare install function executed by Vue.use()
    function install(Vue) {
    	if (install.installed) { return; }
    	install.installed = true;
    	Vue.component('DeleteButton', component);
    }

    // Create module definition for Vue.use()
    var plugin = {
    	install: install,
    };

    // Auto-install when vue is found (eg. in browser via <script> tag)
    var GlobalVue = null;
    if (typeof window !== 'undefined') {
    	GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
    	GlobalVue = global.Vue;
    }
    if (GlobalVue) {
    	GlobalVue.use(plugin);
    }

    exports.install = install;
    exports.default = component;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
